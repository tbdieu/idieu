require "rails_helper"

RSpec.feature "Feed Feature: ", :type => :feature do
  before(:each) do
    @feed1 = create(:feed)
    @feed2 = create(:feed)
    @feed3 = create(:feed)
  end

  scenario "when go to page list of feeds, I should see feeds" do
    visit feeds_path
    expect(page).to have_text(@feed1.title)
    expect(page).to have_text(@feed2.title)
    expect(page).to have_text(@feed3.title)
  end

  scenario "when go to page detail of feed, I should see feed" do
    visit feed_path(@feed1)
    expect(page).to have_text(@feed1.title)
  end
end