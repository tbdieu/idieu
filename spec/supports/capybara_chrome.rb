require 'selenium/webdriver'
require 'capybara/rspec'
# require 'capybara-screenshot/rspec'

RSpec.configure do
  Capybara.server = :puma
  Capybara.server_port = 5000

  # chrome / headless_chrome
  Capybara.register_driver(:headless_chrome) do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: { args: %w(no-sandbox headless disable-gpu) }
    )

    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      desired_capabilities: capabilities
    )
  end
  
  # Capybara.configure do |config|
  #   config.default_max_wait_time = 10 # seconds
  #   config.default_driver        = :chrome
  #   # config.server = :puma, { Silent: true }
  # end

  # Capybara::Screenshot.register_driver(:chrome) do |driver, path|
  #   driver.browser.save_screenshot(path)
  # end

  # chrome / headless_chrome
  Capybara.javascript_driver = :headless_chrome
end