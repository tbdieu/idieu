FactoryBot.define do
  factory :feed do
    title {FFaker::Book.title}
    url {FFaker::Internet.http_url}
    content {FFaker::Book.description}
  end
end