require 'rails_helper'

RSpec.describe Feed, :type => :model do
  context "create feed" do
    before(:each) do
      @params = {title: 'Title', 
                url: 'https://google.com', 
                remote_image_url: 'https://temp-images-141.s3-ap-southeast-1.amazonaws.com/320x480.jpg', 
                content: 'Content'}
    end

    it "successfully with valid params" do
      feed = Feed.create(@params)
      expect(feed.errors.blank?).to eq(true)
      expect(feed.image.thumb.url).not_to be_empty
    end

    it "fail with no title" do
      feed = Feed.create(@params.except(:title))
      expect(feed.errors[:title].present?).to eq(true)
    end

    it "fail with no url" do
      feed = Feed.create(@params.except(:url))
      expect(feed.errors[:url].present?).to eq(true)
    end
  end
end
