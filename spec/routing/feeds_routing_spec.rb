require "rails_helper"

RSpec.describe FeedsController, :type => :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/feeds").to route_to("feeds#index")
    end

    it "routes to #show" do
      expect(:get => "/feeds/1").to route_to("feeds#show", :id => "1")
    end
  end
end
