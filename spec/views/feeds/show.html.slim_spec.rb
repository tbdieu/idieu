require "rails_helper"

RSpec.describe "feeds/show" do
  include Draper::ViewHelpers

  before(:each) do
    @params = {title: 'Title1', 
               url: 'https://google.com', 
               remote_image_url: 'https://temp-images-141.s3-ap-southeast-1.amazonaws.com/320x480.jpg', 
               content: 'Content'}
  end

  it "displays detail of feed" do
    feed = Feed.create!(@params)

    assign(:feed, feed)

    render

    expect(rendered).to match(/#{@params1[:title]}/)
  end
end