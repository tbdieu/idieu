require "rails_helper"

RSpec.describe "feeds/index" do
  include Draper::ViewHelpers

  before(:each) do
    @params1 = {title: 'Title1', 
              url: 'https://google.com', 
              remote_image_url: 'https://temp-images-141.s3-ap-southeast-1.amazonaws.com/320x480.jpg', 
              content: 'Content'}

    @params2 = {title: 'Title2', 
              url: 'https://vnexpress.net', 
              remote_image_url: 'https://temp-images-141.s3-ap-southeast-1.amazonaws.com/320x480.jpg', 
              content: 'Content'}

  end

  it "displays list of feeds" do
    Feed.create!(@params1)
    Feed.create!(@params2)

    assign(:feeds, FeedDecorator.decorate_collection(Feed.page(params[:page])))

    render

    expect(rendered).to match(/#{@params1[:title]}/)
    expect(rendered).to match(/#{@params2[:title]}/)
  end
end