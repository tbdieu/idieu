# frozen_string_literal: true

class CreateFeeds < ActiveRecord::Migration[5.2]
  def change
    create_table :feeds do |t|
      t.string :title
      t.string :image
      t.text :content
      t.string :url, index: :uniq

      t.timestamps
    end
  end
end
