# frozen_string_literal: true

class FeedDecorator < ApplicationDecorator
  delegate_all

  def short_content
    HTML_Truncator.truncate(object.content, ENV['AMOUNT_SHORT_CONTENT_STRING'].to_i)
  end
end
