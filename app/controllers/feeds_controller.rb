# frozen_string_literal: true

class FeedsController < ApplicationController
  def index
    @feeds = FeedDecorator.decorate_collection(Feed.page(params[:page]))
  end

  def show
    @feed = get_feed
  end

  private

  def get_feed
    Feed.find(params[:id])
  end
end
