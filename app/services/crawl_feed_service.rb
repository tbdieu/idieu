# frozen_string_literal: true

require 'open-uri'

class CrawlFeedService
  REST_TIME_FAIL = 1
  AMOUNT_FAIL_TRY = 2
  NP_BLOCK_ITEMS = 'block_items'
  NP_ITEM = 'item'
  NP_COMMAND = '@'

  YCOMBINATOR_STRUCTURE = {
    'news.ycombinator.com/best' => {
      NP_BLOCK_ITEMS => '.itemlist .athing',
      NP_ITEM => {
        'title' => '.title a@content',
        'url' => '.title a@[href]'
      }
    }
  }.freeze

  STRUCTURE_PAGES = YCOMBINATOR_STRUCTURE

  def crawl(url = 'https://news.ycombinator.com/best')
    structure = parse_url_structure(url)
    doc = Nokogiri::HTML(open(url))
    parse_document(doc, structure)
  end

  def parse_url_structure(url)
    uri = URI.parse(url)
    host_path = "#{uri.host}#{uri.path}"
    YCOMBINATOR_STRUCTURE.each do |matching_url, structure|
      return structure if host_path == matching_url
    end

    raise "Not found structure feed for url: #{url}"
  end

  def parse_document(doc, structure)
    doc.search(structure[NP_BLOCK_ITEMS]).each do |item_doc|
      obj = {}
      structure[NP_ITEM].each do |field_name, target|
        mapping_el = target.split(NP_COMMAND)[0]
        command = target.split(NP_COMMAND)[1]
        obj[field_name] = get_el_value(item_doc.search(mapping_el).first, command)
      end

      do_job { crawl_content(obj) }
      save_feed(obj)
    end
  end

  def crawl_content(obj)
    doc = Nokogiri::HTML(open(obj['url']))
    rbody = Readability::Document.new(doc, tags: %w[img], min_image_width: 500)
    obj['remote_image_url'] = rbody.images.first
    obj['content'] = rbody.content
  rescue StandardError => e
    p "Error!!! crawl_content: #{e.to_json}. #{obj['url']}"
  end

  def get_el_value(el, command)
    if command.include?('[')
      el.send(:[], command.tr('[]', ''))
    elsif command.present?
      el.send(command)
    end
  end

  def save_feed(obj)
    Feed.create(obj) if Feed.find_by(url: obj['url']).nil?
  end

  def do_job
    result = nil
    (1..AMOUNT_FAIL_TRY).each do |i|
      result = yield
      break unless result.nil?

      p "fail #{i}"
      sleep(REST_TIME_FAIL)
    end

    result
  end
end
