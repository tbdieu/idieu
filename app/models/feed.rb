# frozen_string_literal: true

class Feed < ApplicationRecord
  validates :url, :title, presence: true

  mount_uploader :image, ImageFeedUploader
end
